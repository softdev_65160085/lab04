/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.OXgame.lab04;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class Game {
    
    private Player player1;
    private Player player2;
    private Table table;

    public Game() {
            this.player1 = new Player('O');
            this.player2 = new Player('X');
        }
    
    public void newGame(){
        this.table = new Table(player1,player2);
    }
    
    public void play(){
        showWelcome();
        newGame();
        while(true){
            showTable();
            showTurn();
            inputNumber();
            if(table.checkWin()){
                showTable();
                showInfo();
                newGame();
            }
            if(table.checkDraw()){
                showTable();
                showInfo();
                newGame();
            }
            table.switchPlayer();
}
    }
    private void showWelcome() {
        System.out.println("Welcome to XO Game");
    }

    private void showTable() {
        char[][] t = table.getTable();
        for(int i = 0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(t[i][j]+ " ");
            }
            System.out.println("");
        }
    }

    private void showTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Turn");
    }

    private void inputNumber() {
    Scanner sc = new Scanner(System.in);
    while (true) {
        System.out.print("Please Input Number to Play :");
        int number = sc.nextInt();
        int col = (number - 1) % 3;
        int row = (number - 1) / 3;
        if (number < 1 || number > 9) {
            System.out.println("Invalid input. Please enter a number between 1 and 9.");
            showTable();
            showTurn();
        }else if(table.setRowCol(row, col) == false){
            System.out.println("Invalid input.");
            showTable();
            showTurn();
        }else {
            table.setRowCol(row, col);
            break;
        }
    }
}
    private void showInfo(){
        System.out.println(player1);
        System.out.println(player2);
    }
}

