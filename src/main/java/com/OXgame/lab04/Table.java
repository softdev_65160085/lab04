/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.OXgame.lab04;

/**
 *
 * @author ASUS
 */
public class Table {
    private char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    private Player player1;
    private Player player2;
    private Player currentPlayer;
    private int count = 0;
    private int row,col;
    public Table(Player player1,Player player2){
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }
    public boolean setRowCol(int row,int col){
        if(table[row][col] == '-'){
            table[row][col] = currentPlayer.getSymbol();
            this.row = row;
            this.col = col;
            return true;
        }
        return false;
    }
    public void switchPlayer(){
        count++;
        if(currentPlayer == player1){
            currentPlayer = player2;
            }else {
            currentPlayer = player1;
        }
    }
    public boolean checkWin(){
        if(checkRow()){
            saveWin();
            return true;
        }else if(checkCol()){
            saveWin();
            return true;
        }else if(checkDiag()){
            saveWin();
            return true;
        }else if(checkAntiDiag()){
            saveWin();
            return true;
        }return false;       
    }
    public boolean checkDraw(){
        if(count == 8){
            saveDraw();
            return true;
        }
    return false;
    }
    public boolean checkRow(){
        return table[row][0] != '-' && table[row][0] == table[row][1] && table[row][2] == table[row][1];
    }
    public boolean checkCol(){
        return table[0][col] != '-' && table[0][col] == table[1][col] && table[2][col] == table[1][col];
    }
    public boolean checkDiag(){
        return table[0][0] != '-' && table[0][0] == table[1][1] && table[2][2] == table[1][1];
    }
    public boolean checkAntiDiag(){
        return table[2][0] != '-' && table[2][0] == table[1][1] && table[0][2] == table[1][1];
    }
        private void saveWin(){
        if(player1 == getCurrentPlayer()){
            player1.win();
            player2.lose();
        }else{
            player2.win();
            player1.lose();
        }
    }
    private void saveDraw(){
        player1.draw();
        player2.draw();
    }
    
}



